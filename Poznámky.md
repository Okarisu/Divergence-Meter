# Poznámky

## TO DO

- hodiny - DS3231
- Divergence meter
- řízení - ON/OFF switch (+relé pro itrony), víc switchů/bluetooth
- mby piezzo - OST?

## Tlačítka

* SWITCH
* čas / divergence
* scramble
* Nastavení - zimní/letní čas

## Součástky

- PCB
- Arduino - Nano
- Itrony
- Tranzistor. pole
- registry
- relé modul (774-028 | 774-022)
- čudlík(y)
- ev. BT modul
- ev. piezzo & SD modul + karta

## Poznámky od Franty

- řádově pár mA - můžu použít ten svůj zdroj
- žhavení paralelně, měnič z 12V na 1,5V, aspoň 1-2A, měnič pro 30V, desítky mA, stabilizátor na 5V
- Frantovy čudlíky - 2 na hýbání nastavení, 2 na in/dektementaci

## Nápady

- bluetooth
- switch - toggle bluetooth
- piezzo - OST